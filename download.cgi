#!/bin/bash

eval $QUERY_STRING

echo "Content-Type: application/octet-stream"

if grep -q '^[[:digit:]]*$' <<< $size; then
        LEN=$(echo "$size * 1024 * 1024" | bc)
else
        exit
fi

echo "Content-Length: $LEN"
echo

dd if=/dev/zero bs=1M count=$size
