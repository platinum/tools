#!/bin/env bash

AUTHOR="PT <platinum@163.com>"
DESCRIPTION="Bandwidth speed limiter"
DATE="2017.11.23"
VERSION="0.1"

IFACE="eth0"
SPEED="0"
MODE="1"
UNIT="bit"
UNIT_INFO="bits/s"

function help() {
cat <<EOF
${DESCRIPTION} v${VERSION} (${DATE})
Author: ${AUTHOR}

Usage: $(basename $0) [-i IFACE] <-s Speed> [-m Mode] [-h] [-v]

        -i IFACE        Default IFACE = eth0
        -s Speed        Bandwidth speed, 0 = no limit, default Speed = 0.
        -m Mode         1 = bps, 2 = B/s, default mode = 1.
        -v              Show version.
        -h              Show this message.

Example: $0 -i eth2 -s 30M -m 1 (eth2, 30Mbps)

EOF
}

function do_limit() {
        if [ "$2" = "0" ]; then
                tc qdisc del dev ${IFACE} root &>/dev/null
                echo "$1 is unlimited."
                return
        fi

        tc qdisc del dev ${IFACE} root &>/dev/null
        tc qdisc add dev ${IFACE} root handle 1: htb default 9999
        tc class add dev ${IFACE} parent 1: classid 1:9999 htb rate ${SPEED}${UNIT} ceil ${SPEED}${UNIT}
}

if [[ $# -lt 2 ]]; then
        help
        exit 1
fi

while getopts "i:s:m:vh" opt; do
        case $opt in
                'i')
                        IFACE="${OPTARG}"
                        ;;
                's')
                        SPEED="${OPTARG}"
                        ;;
                'm')
                        MODE="${OPTARG}"
                        if [ "${MODE}" == "1" ]; then
                                UNIT="bit"
                                UNIT_INFO="bps"
                        else
                                UNIT="bps"
                                UNIT_INFO="B/s"
                        fi
                        ;;
                'v')
                        echo "Version: ${VERSION}"
                        exit
                        ;;
                'h')
                        help
                        exit
                        ;;
                '?')
                        echo "Invalid option: -${OPTARG}"
                        exit 1
                        ;;
        esac
done

echo "${IFACE}: ${SPEED}${UNIT_INFO}"
do_limit ${IFACE} ${SPEED} ${UNIT}
echo "Done."
