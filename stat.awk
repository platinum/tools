#!/bin/awk

BEGIN {
	min = 2 ^ 63;
	max = -(2 ^ 63);
	sum = 0;
	DEBUG = 0;
}

{
	# Read value and save to array named arr1
	arr1[NR] = $1;

	# Minimum
	if (min > $1)
		min = $1;

	# Maximum
	if (max < $1 && $1 > -(2^63) && $1 < 2^63)
		max = $1;

	# Sum
	sum += $1;
}

END {
	# Sort arr1 and save to arr2
	asort(arr1, arr2);

	if (DEBUG) {
		for (i=1; i<=NR; i++)
			printf("arr1[%d]: %d\n", i, arr1[i]);
		for (i=1; i<=NR; i++)
			printf("arr2[%d]: %d\n", i, arr2[i]);
	}

	if (NR % 2 > 0) {
		# If odd number
		med = arr2[int(NR / 2) + 1];
		if (DEBUG)
			printf("MED: %.3f\n", arr2[int(NR / 2) + 1]);
	} else {
		# if even number
		med = (arr2[int(NR / 2)] + arr2[int(NR / 2 + 1)]) / 2;
		if (DEBUG)
			printf("MED: (%d + %d) / 2 = %.3f\n",
				arr2[int(NR / 2)], arr2[int(NR / 2 + 1)],
				(arr2[int(NR / 2)] + arr2[int(NR / 2 + 1)]) / 2);
	}

	# Average
	avg = sum / NR;

	printf("MAX: %.3f, MIN: %.3f, AVG: %.3f, MED: %.3f\n",
		max, min, avg, med);
}
