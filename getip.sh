#!/bin/env bash
 
# Author: Jin Bai(PT) <platinum@163.com>
# Description: This is a shell script demo frame.
# Date: 2015.11.24
 
VERSION="0.1"
 
help() {
        echo -e "\nCollect IPs from webpage (mdig, 17ce, ipip) v$VERSION by PT (platinum@fastweb.com.cn).\n"
        echo "Usage: $(basename $0) -t <logtype> -f <logfile> [-h] [-v]"
cat <<EOF
        -t <logtype>    Choose format type. ("mdig" or "17ce" or "ipip")
        -f <logfile>    Log file from webpage.
        -v              Show version.
        -h              Show this message.

Example:
        `basename $0` -t 17ce -f ~/17ce.log

EOF
}
 
if [[ $# -lt 2 ]]; then
        help
        exit 1
fi
 
while getopts "t:f:vh" opt; do
        case $opt in
                't')
                        if [ "${OPTARG}" = "17ce" ] || [ "${OPTARG}" = "ipip" ] || [ "${OPTARG}" = "mdig" ]; then
                                TYPE="${OPTARG}"
                        else
                                help
                                exit
                        fi
                        ;;
                'f')
                        FILE="${OPTARG}"
                        ;;
                'v')
                        echo "Version: ${VERSION}"
                        exit
                        ;;
                'h')
                        help
                        exit
                        ;;
                '?')
                        echo "Invalid option: -${OPTARG}"
                        exit 1
                        ;;
        esac
done
 
if [ "${TYPE}" = "17ce" ]; then
        cat $FILE | grep -o '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*' | sort -u
elif [ "${TYPE}" = "ipip" ]; then
        cat $FILE | grep -o '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*(' | sed 's/(//' |  sort -u
elif [ "${TYPE}" = "mdig" ]; then
        cat $FILE | awk '{print $NF}' | grep -o '[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*(' | sed 's/(//' |  sort -u
fi
